local entry = {}

function entry.of(table, key)
    local mt = { __index = entry }
    return setmetatable({
        table = table,
        key = key,
    }, mt)
end

function entry:get_or_default(default)
    local value = self.table[self.key]
    if value == nil then
        return default
    else
        return value
    end
end

function entry:get_or_set_default(default)
    local value = self.table[self.key]
    if value == nil then
        self.table[self.key] = default
        return default
    else
        return value
    end
end

return entry.of

