-- tape: Twin escAPE
local push = _G.table.insert or _G.array.push

local function lex(src, pos) --> Option<token>, next_pos
    if pos == #src + 1 then
        return nil, pos
    end

    if pos > #src + 1 then
        error(("Invalid position %d: input length is only %d"):format(pos, #src))
    end

    local match, token

    match, token = src:match("^(%[(%[))", pos)
    if match ~= nil then
        return token, pos + #match
    end

    match, token = src:match("^(%](%]))", pos)
    if match ~= nil then
        return token, pos + #match
    end

    match, token = src:match("^(%[([^][]-)%])", pos)
    if match ~= nil then
        return token, pos + #match
    end

    if src:sub(pos, pos) == "[" then
        error(("Missing `]` for the corresponding `[` at %d.\n\z
                Use `[[` if you intend for a single `[` character."):format(pos))
    end

    if src:sub(pos, pos) == "]" then
        error(("Missing `[` for the corresponding `]` at %d.\n\z
                Use `]]` if you intend for a single `]` character."):format(pos))
    end

    return src:sub(pos, pos), pos + 1
end

local function parse(src)
    local pos = 1
    local result = {}
    local token

    while true do
        token, pos = lex(src, pos)
        if token == nil then
            return result
        else
            push(result, token)
        end
    end
end

return parse

