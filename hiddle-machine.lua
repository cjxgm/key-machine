local hm = {}
local push = _G.table.insert or _G.array.push

local function abs(x)
    if x < 0 then x = -x end
    return x
end

local function is_key(ev, code, down)
    if ev.event ~= "key" then return false end
    if ev.code ~= code then return false end
    if down ~= nil then
        if down and ev.value == 0 then return false end
        if not down and ev.value ~= 0 then return false end
    end
    return true
end

local function is_rel(ev, code)
    if ev.event ~= "rel" then return false end
    if ev.code ~= code then return false end
    return true
end

local function push_key(events, code, down)
    local value = 0
    if down then value = 1 end
    push(events, { event = "key", code = code, value = value })
end

local function push_rel(events, code, motion)
    push(events, { event = "rel", code = code, value = motion })
end

local function push_key_press(events, code)
    push_key(events, code, true)
    push_key(events, code, false)
end

local function switch(x, arms)
    for name, fn in pairs(arms) do
        if name == x then
            fn()
            return
        end
    end
    error(("Invalid value %q, no arm matched."):format(x))
end

function hm.new(scroll_delay, drag_threshold, verbose)
    local mt = { __index = hm }
    return setmetatable({
        mode = 'normal',
        pending_start_time = 0,
        pending_dragged_distance = 0,
        scroll_delay = scroll_delay,
        drag_threshold = drag_threshold,
        verbose = verbose or false,
    }, mt)
end

function hm:feed(ev)
    local events = {}

    switch(self.mode, {
        normal = function()
            if is_key(ev, "btn_middle", true) then
                if self.verbose then print("normal -> pending") end
                self.mode = 'pending'
                self.pending_start_time = ev.seconds
                self.pending_dragged_distance = 0
            else
                push(events, ev)
            end
        end,
        pending = function()
            if is_rel(ev, "x") or is_rel(ev, "y") then
                self.pending_dragged_distance = self.pending_dragged_distance + abs(ev.value)
                local drag_timed_out = ev.seconds - self.pending_start_time > self.scroll_delay
                local drag_exceeds_threshold = self.pending_dragged_distance > self.drag_threshold

                if drag_timed_out then
                    if self.verbose then print("pending -> scroll") end
                    self.mode = 'scroll'
                elseif drag_exceeds_threshold then
                    if self.verbose then print("pending -> drag") end
                    self.mode = 'drag'
                    push_key(events, "btn_middle", true)
                end
            elseif is_key(ev, "btn_middle", false) then
                if self.verbose then print("pending -> normal", "[click]") end
                self.mode = 'normal'
                push_key_press(events, "btn_middle")
            else
                push(events, ev)
            end
        end,
        drag = function()
            push(events, ev)
            if is_key(ev, "btn_middle", false) then
                if self.verbose then print("drag -> normal") end
                self.mode = 'normal'
            end
        end,
        scroll = function()
            if is_rel(ev, "x") or is_rel(ev, "y") then
                if self.scrolls == nil then self.scrolls = {} end
                if self.scrolls[ev.code] ~= nil then
                    self.scrolls.x = 0
                    self.scrolls.y = 0
                end
                self.scrolls[ev.code] = ev.value
                if self.scrolls.x ~= nil and self.scrolls.y ~= nil then
                    if self.verbose then print("scroll", self.scrolls.x, self.scrolls.y) end
                    local ax = abs(self.scrolls.x)
                    local ay = abs(self.scrolls.y)
                    if ax >= ay then push_rel(events, "hwheel", self.scrolls.x) end
                    if ay >= ax then push_rel(events, "wheel", -self.scrolls.y) end
                    self.scrolls = nil
                end
            elseif is_key(ev, "btn_middle", false) then
                if self.verbose then print("scroll -> normal") end
                self.mode = 'normal'
            else
                push(events, ev)
            end
        end,
    })

    return events
end

return hm.new

