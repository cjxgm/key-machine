local hm = require [[hiddle-machine]]
local input = _G.input

local function auto_syn(s)
    return s:filter(function(ev)
        return ev.event ~= 'syn'
    end)
    :map(function(ev)
        return { ev, { event = "syn" } }
    end)
    :unfold()
end

local function no_msc_syn(s)
    return s:filter(function(ev)
        return ev.event ~= 'msc' and ev.event ~= 'syn'
    end)
end

hm = hm(0.5, 20)

return {
    mouse = auto_syn(
        no_msc_syn(input.mouse)
            :map(function(ev)
                return hm:feed(ev)
            end)
            :unfold()
    ),
}

