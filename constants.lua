local tape = require [[tape]]
local consts = {}

local function split_to_map(src)
    local result = {}
    for k, v in src:gmatch[[(%S+)%s+(%S+)]] do
        result[k] = v
    end
    return result
end

local function split_tape_to_map(src)
    local result = {}
    for k1, k2 in src:gmatch[[(%S+)%s+(%S+)]] do
        local keys1 = tape(k1)
        local keys2 = tape(k2)
        for i=1,#keys1 do
            result[keys1[i]] = keys2[i]
        end
    end
    return result
end

local function reverse_map(m, prefer)
    local result = {}
    for k, v in pairs(m) do
        local preferred_key = k
        if result[v] ~= nil then
            preferred_key = prefer(preferred_key, result[v])
        end
        result[v] = preferred_key
    end
    return result
end

local function make_set(arr)
    local result = {}
    for _, v in ipairs(arr) do
        result[v] = true
    end
    return result
end

consts.keys = tape "\z
        [grave]1234567890[minus][equal][backspace]\z
        [tab]qwertyuiop[leftbrace][rightbrace][backslash]\z
        [capslock]asdfghjkl[semicolon][apostrophe][enter]\z
        [leftshift]zxcvbnm[comma][dot][slash][rightshift]\z
        [leftctrl][leftmeta][leftalt][space][rightalt][menu][rightmeta][rightctrl]\z
        [up][down][left][right][home][end][pageup][pagedown]\z
        [insert][delete][sysrq][pause][scrolllock]\z
        [esc][f1][f2][f3][f4][f5][f6][f7][f8][f9][f10][f11][f12]\z
        [numlock][kp0][kp1][kp2][kp3][kp4][kp5][kp6][kp7][kp8][kp9]\z
        [kpdot][kpenter][kpplus][kpminus][kpasterisk][kpslash]\z
        [volumeup][volumedown][min_interesting]\z
        [brightnessup][brightnessdown]\z
        [previoussong][nextsong][playpause]\z
        [wakeup]\z
"

consts.pretty_keys = split_to_map "\z
        kpenter kpreturn    kpplus kp+      kpminus kp-\n\z
        kpasterisk kp*      kpslash kp/     kpdot kp.\n\z
        enter return        grave `     minus -     equal =\n\z
        leftbrace [         rightbrace ]    backslash \\\n\z
        semicolon ;         apostrophe '\n\z
        comma ,     dot .   slash /\n\z
        leftshift shift     rightshift shift\n\z
        leftctrl ctrl       rightctrl ctrl\n\z
        leftalt alt         rightalt alt\n\z
        leftmeta super      rightmeta super\n\z
        volumeup volume+    volumedown volume-      min_interesting mute\n\z
        brightnessup brightness+                    brightnessdown brightness-\n\z
        previoussong song<  nextsong song>          playpause play\n\z
"

consts.shifted_keys = split_tape_to_map "\z
        `1234567890-=\n\z
        ~!@#$%^&*()_+\n\z
        \z
        qwertyuiop[[]]\\\n\z
        QWERTYUIOP{}|\n\z
        \z
        asdfghjkl;'\n\z
        ASDFGHJKL:\"\n\z
        \z
        zxcvbnm,./\n\z
        ZXCVBNM<>?\n\z
        \z
"

consts.ugly_keys = reverse_map(consts.pretty_keys, function (a, b)
    if a:match("^left") then
        return a
    else
        return b
    end
end)

consts.unshifted_keys = reverse_map(consts.shifted_keys, function (a, b)
    error(("Duplicated value for key %q and %q"):format(a, b))
end)

consts.keys_set = make_set(consts.keys)

return consts

