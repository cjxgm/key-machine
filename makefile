
all: build/graph.lua build/machine.lua build/compose-to-mmb.lua build/hiddle.lua
clean:
	rm -rf build/
rebuild: clean
	@$(MAKE) --no-print-directory all
build/:
	mkdir -p $@
build/%.lua: %.lua | build/
	./package.sh $^ > $@
test: all
	cp build/graph.lua /tmp/graph.lua
	cp build/machine.lua /tmp/machine.lua
	cp build/compose-to-mmb.lua /tmp/compose-to-mmb.lua
	cp build/hiddle.lua /tmp/hiddle.lua
	/usr/lib/evmap/evmapd --user -vE /tmp/graph.lua

build/graph.lua: graph.lua tape.lua constants.lua entry.lua
build/machine.lua: machine.lua tape.lua triggers.lua entry.lua conversion.lua constants.lua key-machine.lua
build/compose-to-mmb.lua: compose-to-mmb.lua
build/hiddle.lua: hiddle.lua hiddle-machine.lua

