#!/bin/sh
cat <<END
local module = {}
local loaded = {}

local function require(name)
    if loaded[name] == nil then
        if module[name] == nil then
            error(("Module %q not found"):format(name))
        end
        loaded[name] = module[name]()
        module[name] = nil
    end

    return loaded[name]
end

local function define(name, chunk)
    if loaded[name] ~= nil or module[name] ~= nil then
        error(("Module %q already exists"):format(name))
    end
    module[name] = chunk
end

END

for path in "$@"; do
    module=$(basename $path .lua)
    echo "define([[$module]], function()"
    cat $path
    echo "end)"
    echo
done

echo "return require [[$(basename $1 .lua)]]"
echo

