local consts = require [[constants]]
local tape = require [[tape]]
local source = _G.source
local group = _G.group
local patch = _G.patch
local array = _G.array
local index = array.index

source {
    keyboard = function(metadata)
        if metadata.events.key == nil then return false end
        local has_key_a_event = index(metadata.events.key, "a") ~= nil
        local has_key_left_event = index(metadata.events.key, "btn_left") ~= nil
        return has_key_a_event and not has_key_left_event
    end,
    mouse = function(metadata)
        if metadata.events.key == nil then return false end
        local has_key_a_event = index(metadata.events.key, "a") ~= nil
        local has_key_left_event = index(metadata.events.key, "btn_left") ~= nil
        return has_key_left_event and not has_key_a_event
    end,
}

group {
    machine = {
        sinks = {
            machine_kbd = {
                name = "Key Machine Keyboard",
                events = {
                    key = consts.keys,
                },
            },
            machine_mouse = {
                name = "Key Machine Mouse",
                events = {
                    key = tape "[btn_left][btn_right][btn_middle]",
                    rel = tape "xy[wheel][hwheel]",
                },
            },
        },
        connections = {
            ["machine_kbd"] = "machine.kbd",
            ["machine.kbd"] = "compose_to_mmb.kbd",
            ["machine_mouse"] = "hiddle.mouse",
            ["hiddle.mouse"] = "compose_to_mmb.mouse",
            ["compose_to_mmb.kbd"] = "keyboard",
            ["compose_to_mmb.mouse"] = "mouse",
        },
    },
}

patch {
    compose_to_mmb = "/tmp/compose-to-mmb.lua",
    machine = "/tmp/machine.lua",
    hiddle = "/tmp/hiddle.lua",
}

