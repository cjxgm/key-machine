local entry = require [[entry]]
local tape = require [[tape]]
local conv = require [[conversion]]
local km = {}

local push = _G.table.insert or _G.array.push


local function push_key(keys, code, down)
    local value = 0
    if down then value = 1 end
    --print("KEYOUT", code, value)
    push(keys, { event = "key", code = code, value = value })
end

local function push_pkey(keys, pretty_code, down)
    push_key(keys, conv.uglify_key(pretty_code), down)
end

local function push_pkey_press(keys, pretty_code)
    push_pkey(keys, pretty_code, true)
    push_pkey(keys, pretty_code, false)
end


function km.new()
    local mt = { __index = km }
    local state = { nexts = {} }
    return setmetatable({
        lshift = false,
        rshift = false,
        ignore_next_keyup = {},
        root_state = state,
        state = state,
    }, mt)
end

function km:feed(key)
    -------- shift --------
    if key.code == "leftshift" then
        self.lshift = key.value == 1
        return { key }
    end
    if key.code == "rightshift" then
        self.rshift = key.value == 1
        return { key }
    end
    if self.lshift and self.rshift then
        return { { event = "syn" } }
    end

    -------- keyup --------
    if key.value == 0 then
        if self.ignore_next_keyup[key.code] then
            self.ignore_next_keyup[key.code] = nil
            return { { event = "syn" } }
        else
            return { key }
        end
    end

    -------- keydown --------
    local keys = { key }
    local pkey = conv.prettify_key(key.code)
    local skey = conv.fuse_shift(pkey, self.lshift or self.rshift)
    --print("KEYIN", skey)
    while true do
        local next_state = self.state.nexts[skey] or self.state.nexts.ANY
        if next_state then
            self.state = next_state
            local action = self.state.action
            if action ~= nil then
                push(keys, { event = "key", code = key.code, value = 0 })
                self.ignore_next_keyup[key.code] = true

                for _, output in ipairs(action()) do
                    self:_push_skey_press(keys, output)
                end
            end
            break
        elseif self.state == self.root_state then
            break
        else
            self.state = self.root_state
        end
    end
    return keys
end

function km:add_trigger(trigger, outputs)
    local trigger_src = trigger
    trigger = tape(trigger)
    outputs = tape(outputs)

    if #trigger == 0 then
        error("Trigger cannot be empty")
    end

    local function action()
        return outputs
    end

    local state = self.root_state
    for _, key in ipairs(trigger) do
        state = entry(state.nexts, key):get_or_set_default({ nexts = {} })
    end

    if state.action == nil then
        state.action = action
    else
        error(("Duplicated trigger %q"):format(trigger_src))
    end
end

function km:_push_skey_press(keys, shiftable_code)
    local pkey, shift = conv.extract_shift(shiftable_code)
    local shift_state = self.lshift or self.rshift
    --print("SKEY", shiftable_code, pkey, shift)
    --print("SHIFT", self.lshift, self.rshift)
    if shift_state == shift then
        push_pkey_press(keys, pkey)
    else
        local shift_key = "leftshift"
        if self.rshift ~= shift then shift_key = "rightshift" end

        push_pkey(keys, shift_key, shift)
        push_pkey_press(keys, pkey)
        push_pkey(keys, shift_key, shift_state)
    end
end

return km.new

