local conv = require [[conversion]]
local km = require [[key-machine]]
local add_triggers = require [[triggers]]
local input = _G.input
local warn = _G.warn

km = km()
add_triggers(function(trigger, outputs)
    km:add_trigger(trigger, outputs)
end)

return {
    kbd = input.kbd
        :filter(function(ev)
            if ev.event == 'key' then
                if ev.value > 1 then return false end
                if conv.key_is_legal(ev.code) then
                    return true
                else
                    warn("Ignored unsupported key %q", ev.code)
                    return false
                end
            end
            return false
        end)

        :map(function(ev)
            return km:feed(ev)
        end)
        :unfold()

        -- legality check
        :filter(function(ev)
            if ev.event == 'syn' then
                return false
            end
            if conv.key_is_legal(ev.code) then
                return true
            else
                warn("Ignored unsupported output key %q", ev.code)
                return false
            end
        end)

        -- syn after every event
        :map(function(ev)
            return { ev, { event = "syn" } }
        end)
        :unfold(),
}

