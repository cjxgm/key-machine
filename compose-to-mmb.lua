local input = _G.input

--          ┌──────────────────┐
--  mouse ──>                  >── mouse
--          │  compose to mmb  │
--    kbd ──>                  >── kbd
--          └──────────────────┘
--
-- all in:mouse goes to out:mouse
-- if in:kbd is compose, "btn_middle" goes to out:mouse
-- if in:kbd is not compose, in:kbd goes to out:mouse

local function inverse(pred)
    return function(x)
        return not pred(x)
    end
end

local function split_filter(s, pred)
    local ss = s:dup(2)
    local yes = ss[1]:filter(pred)
    local no = ss[2]:filter(inverse(pred))
    return { yes = yes, no = no }
end

local function is_compose(ev)
    return ev.event == 'key' and ev.code == 'compose'
end

local function is_mouse_event(ev)
    if ev.event == 'rel' then
        if ev.code == 'x' then return true end
        if ev.code == 'y' then return true end
        if ev.code == 'wheel' then return true end
        if ev.code == 'hwheel' then return true end
        return false
    end
    if ev.event == 'key' then
        if ev.code == 'btn_left' then return true end
        if ev.code == 'btn_right' then return true end
        if ev.code == 'btn_middle' then return true end
        return false
    end
    return false
end

local function is_keyboard_event(ev)
    if ev.event == 'rel' then return false end
    if ev.event == 'key' then
        if ev.code == 'btn_left' then return false end
        if ev.code == 'btn_right' then return false end
        if ev.code == 'btn_middle' then return false end
        return true
    end
    return false
end

local function auto_syn(s)
    return s:filter(function(ev)
        return ev.event ~= 'syn'
    end)
    :map(function(ev)
        return { ev, { event = "syn" } }
    end)
    :unfold()
end

local function no_msc(s)
    return s:filter(function(ev)
        return ev.event ~= 'msc'
    end)
end

local kbd_compose_or_not = split_filter(no_msc(input.kbd), is_compose)
local mouse_keyboard = input.mouse:dup(2)
local mouse = mouse_keyboard[1]:filter(is_mouse_event)
local keyboard = mouse_keyboard[2]:filter(is_keyboard_event)

return {
    kbd = auto_syn(kbd_compose_or_not.no:merge(keyboard)),
    mouse = auto_syn(
        mouse:merge(
            kbd_compose_or_not.yes:map(function(ev)
                return {
                    event = "key",
                    code = "btn_middle",
                    value = ev.value,
                    seconds = ev.seconds,
                }
            end)
        )
    ),
}

