local consts = require [[constants]]
local entry = require [[entry]]

local pretty_keys = consts.pretty_keys
local ugly_keys = consts.ugly_keys
local shifted_keys = consts.shifted_keys
local unshifted_keys = consts.unshifted_keys
local keys_set = consts.keys_set

local conv = {}

function conv:prettify_key()
    return entry(pretty_keys, self):get_or_default(self)
end

function conv:uglify_key()
    return entry(ugly_keys, self):get_or_default(self)
end

function conv:fuse_shift(shift)
    if shift and shifted_keys[self] ~= nil then
        return shifted_keys[self]
    else
        return self
    end
end

function conv:extract_shift() --> unshifted_key, shift
    if unshifted_keys[self] ~= nil then
        return unshifted_keys[self], true
    else
        return self, false
    end
end

function conv:key_is_legal()
    return keys_set[self] ~= nil
end

return conv

